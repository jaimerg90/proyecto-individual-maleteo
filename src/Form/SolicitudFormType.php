<?php

namespace App\Form;

use App\Entity\Solicitud;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SolicitudFormType extends AbstractType{

    //Pinta el formulario de las solicitudes
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre');
        $builder->add('email');
        $builder->add('horario');
        $builder->add('ciudad', ChoiceType::class, [
            'choices' => [
                'Madrid' => 'madrid',
                'Teruel' => 'teruel',
                'Sevilla' => 'sevilla'
            ],
            'placeholder' => 'Elige una opcion'
        ]);
        $builder->add('privacidad', CheckboxType::class, [
            'label' => 'He leído y acepto la Política de Privacidad',
            'required' => 'true'
        ]);
        $builder->add('Enviar', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Solicitud::class]);
    }
}