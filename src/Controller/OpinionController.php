<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Opinion;
use Symfony\Component\HttpFoundation\RedirectResponse;

class OpinionController extends AbstractController
{
    //Método que inserta opiniones en la bbdd
    /**
     * @Route("/opinion/add", name="add-opinion")
     */
    public function addOpinion(EntityManagerInterface $em){

        $opinion = new Opinion();
        $opinion->setComentario('');
        $opinion->setNombre('');
        $opinion->setApellido('');
        $opinion->setBarrio('');
        $opinion->setCiudad('');


        $em->persist($opinion);
        $em->flush();

        return $this->render('opinion/index.html.twig');
    }

}   

