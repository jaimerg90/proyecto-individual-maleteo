<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Opinion;
use Symfony\Component\HttpFoundation\Request;
use App\Form\SolicitudFormType;

class IndexController extends AbstractController{

    /**
     * @Route("/", name="homepage")
     */
    public function index(EntityManagerInterface $em, Request $request){

        $rep = $em->getRepository(Opinion::class);
        $opinion = $rep->findBy([],['likes' => 'DESC'], 3);

        $form = $this->createForm(SolicitudFormType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $solicitud = $form->getData();

            $em->persist($solicitud);
            $em->flush();

            return $this->redirectToRoute('homepage');
     
        }

        return $this->render('maleteo.html.twig', ['formulario' => $form->createView(), 'opinion' => $opinion]);
    }

    
}