<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Usuario;
use App\Entity\Solicitud;
use App\Form\UsuarioFormType;
use Symfony\Component\HttpFoundation\Request;

class UsuarioController extends AbstractController
{   
    //Método para introducir usuarios en la bbdd
    /**
     * @Route("/usuario/add", name="add-usuario")
     */
    public function addUser(EntityManagerInterface $em){
        $usuario = new Usuario();
        $usuario->setEmail('pepe@pepe');
        $usuario->setPassword('pepe');
        $usuario->setRole('User');

        $em->persist($usuario);
        $em->flush();

        return $this->render('usuario/index.html.twig');
    }

    //Método que lista todas las solicitudes y gestiona el formulario. Devuelve el listado de solicitudes si el usuario es administrador y el index si no lo es.
    /**
     * @Route("/login", name="login-usuario", methods={"GET", "POST"})
     */
    public function login(Request $request, EntityManagerInterface $em){

        
        $form = $this->createForm(UsuarioFormType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $usuario = $form->getData();
            $username = $usuario->getEmail();
            $password = $usuario->getPassword();

            $rep = $em->getRepository(Usuario::class);
            $usuario = $rep->findOneBy(['email' => $username, 'password' => $password]);
            $role = $usuario->getRole();

            if($role === 'Admin'){                
                return $this->redirectToRoute('list-solicitud');

            }else{
                return $this->redirectToRoute('homepage');
            };
        }

        return $this->render('usuario/index.html.twig', ['formulario' => $form->createView()]);
    }
}
