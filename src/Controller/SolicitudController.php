<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Solicitud;
use App\Form\EditSolicitudFormType;

class SolicitudController extends AbstractController
{
    /**
     * @Route("solicitud/listado", name="list-solicitud")
     */
    public function list(EntityManagerInterface $em){

        $rep = $em->getRepository(Solicitud::class);
        $solicitud = $rep->findAll();

        return $this->render('solicitud/index.html.twig', ['solicitud' => $solicitud]);
    }

    /**
     * @Route("/remove/{id}", name="remove-solicitud")
     */
    public function remove(Solicitud $solicitud, EntityManagerInterface $em){
       
        $em->remove($solicitud);
        $em->flush();

        $this->addFlash('success', 'La solicitud se ha eliminado con éxito');
        return $this->redirectToRoute('list-solicitud', ['solicitud' => $solicitud]);
    }

    /**
     * @Route("/edit/{id}", name="edit-solicitud")
     */
    public function edit(Solicitud $solicitud, EntityManagerInterface $em, Request $request){

        $form = $this->createForm(EditSolicitudFormType::class, $solicitud);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $solicitud = $form->getData();

            $em->flush();
            
            $this->addFlash('success', 'La solicitud se ha editado con éxito');
            return $this->redirectToRoute('list-solicitud');
        }

        return $this->render('solicitud/edit.html.twig', ['formulario' => $form->createView()]);
    }
}   

