const pintacaja = () => {
    const formulario = document.getElementById('formulariopint')
    formulario.classList.add('form-image__disappear');

    const gdiv = document.createElement('div');
    gdiv.classList.add('thanks');
    
    const title = document.createElement('h1');
    title.classList.add('thanks__title');
    title.innerText = '¡Gracias!';

    const parrafo = document.createElement('p');
    parrafo.classList.add('thanks__parrafo');
    parrafo.innerText = 'Tu formulario se ha enviado correctamente, pronto serás un guardián de maleteo'
    
    document.getElementById('aaa').appendChild(gdiv);
    gdiv.appendChild(title);
    gdiv.appendChild(parrafo);
}

document.addEventListener("DOMContentLoaded", () => {
    const form = document.getElementById('formulario');

    form.addEventListener('submit', (event) => {
        event.preventDefault();

        const formdata = new FormData(event.target);
        
        fetch(form.getAttribute('action'),{
            method: form.getAttribute('method'),
            body: formdata
        }).then((res) => {
            if(res.ok){
                pintacaja();
            }else{
                console.log('Error')
            }
        })
    })

    const templateUser = '{{each(options)}}<div class="map__user"><ul class="map__list"><li class="name-user">{{@this.name}}</li><li class="rating-user">{{@this.rating}}</li><li class="size-user">{{@this.size}}</li><img src="assets/images/male.svg"></ul></div>{{/each}}';

    const pintalistado = (obj) => {
        console.log(obj)
        const listado = Sqrl.Render(templateUser, obj)
        document.getElementById('map__users').innerHTML = listado
    }

    fetch('https://jsonblob.com/api/jsonBlob/68f0896f-224a-11ea-b6f9-fda03b8457bd').then(res => {
        return res.json()
    }).then(pintalistado)
})


       