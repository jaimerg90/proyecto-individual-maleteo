<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* maleteo.html.twig */
class __TwigTemplate_521671884d3ee355a2b540e4e602a3e16fcb817831e97a9d36ddbd88204a2e34 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "maleteo.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "maleteo.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,700&display=swap\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"assets/styles/styles.css\">
</head>
<body>
    <div class=\"wrapper\">
        <div class=\"content-fluid\">
            <header class=\"header\">
                <div class=\"header__image\">
                    <img src=\"assets/images/logo.svg\"
                         alt=\"Maleteo\">
                </div>
            </header>
            <div class=\"intro\">
                <div class=\"row\">
                    <div class=\"col-md-6  col-xs-12\">
                        <div class=\"intro__content\">
                            <div class=\"intro__things\">
                                <h1 class=\"intro__title heading-1\">Gana dinero guardando equipaje a viajeros como tú</h1>
                                <ul class=\"intro__app-images\">
                                    <li class=\"intro__app-item\">
                                        <img src=\"assets/images/app-store.svg\" alt=\"App store\" class=\"intro__app-btn\">
                                    </li>
                                    <li class=\"intro__app-item\">
                                        <img src=\"assets/images/google-play.svg\" alt=\"Google play\" class=\"intro__app-btn\">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"function\">
                <h2 class=\"function__title heading-2\">¿Cómo funciona?</h2>
                <ul class=\"function__list row\">
                    <li class=\"col-xs-12 col-md-4\">
                        <div class=\"function__block\">
                            <div class=\"ball\">1</div>
                            <div class=\"function__text-content\">
                                <h2 class=\"function__subtitle\">Date de alta</h2>
                                <p class=\"function__text\">Lorem ipsum dolor sit, recusandae corporis libero cupiditate qui facilis voluptatibus et perferendis. Corporis non ullam voluptatem doloremque accusantium nam eveniet repellat.</p>
                            </div>
                        </div>
                    </li>
                    <li class=\"col-xs-12 col-md-4\">
                        <div class=\"function__block\">
                            <div class=\"ball\">2</div>
                            <div class=\"function__text-content\">
                                <h2 class=\"function__subtitle\">Publica tus espacios, horarios y tarifas</h2>
                                <p class=\"function__text\">Lorem ipsum dolor sit, recusandae corporis libero cupiditate qui facilis voluptatibus et perferendis. Corporis non ullam voluptatem doloremque accusantium nam eveniet repellat.</p>
                            </div>
                        </div>
                    </li>
                    <li class=\"col-xs-12 col-md-4\">
                        <div class=\"function__block\">
                            <div class=\"ball\">3</div>
                            <div class=\"function__text-content\">
                                <h2 class=\"function__subtitle\">Recibe viajeros y gana dinero</h2>
                                <p class=\"function__text\">Lorem ipsum dolor sit, recusandae corporis libero cupiditate qui facilis voluptatibus et perferendis. Corporis non ullam voluptatem doloremque accusantium nam eveniet repellat.</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class=\"form-image\">
                <div class=\"row\">
                    <div class=\"col-md-5\">
                        <div class=\"form-image__image\">
                            <img src=\"assets/images/iPhoneX.png\" alt=\"Image de un móvil\">
                        </div>
                    </div>
                    <div class=\"col-md-6 col-md-offset-1 col-xs-12\">
                        <div class=\"form-image__form\">
                            <div class=\"form-container\">
                                <p class=\"form-image__title heading-1\">Solicita una demo</p>
                                <form action=\"\" method=\"\">
                                    <div class=\"form-image__label-form heading-3\">
                                        <label class=\"label-form__text\" for=\"name\">Nombre</label>
                                        <input type=\"text\">
                                    </div>
                                    <div class=\"form-image__label-form heading-3\">
                                        <label class=\"label-form__text\" for=\"email\">Email</label>
                                        <input type=\"email\">
                                    </div>
                                    <div class=\"form-image__label-form heading-3\">
                                        <label class=\"label-form__text\" for=\"name\">Horario preferido</label>
                                        <input type=\"text\">
                                    </div>
                                    <div class=\"form-image__label-form heading-3\">
                                        <label class=\"label-form__text\" for=\"city\">Ciudad</label>
                                        <select name=\"city\" id=\"city\">
                                            <option value=\"\">Madrid</option>
                                        </select>
                                    </div>
                                    <div class=\"form-image__privacity heading-3\">
                                        <input type=\"checkbox\">
                                        <label>He leído y acepto la Política de Privacidad</label>
                                    </div>
                                    <div>
                                        <a href=\"#\" class=\"form-image__btn\">Enviar</a>
                                    </div>
                                </form>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"opinions\">
                <h2 class=\"function__title heading-2\">Opiniones de otros guardianes</h2>
                <ul class=\"row\">
                    <li class=\"col-md-4 col-xs-12\">
                        <div class=\"opinions__content\">
                            <div class=\"opinions__text heading-3\">
                                <p>Nihil nam molestiae, animi ipsam ex labore? Expedita tenetur tempora mollitia, dolor blanditiis rerum eaque quia vero repellendus natus eveniet vel quam!</p>
                                <div class=\"triangulo-container\">
                                    <div class=\"triangulo-base\"></div>
                                    <div class=\"triangulo\"></div>
                                </div>
                            </div>
                            <div class=\"opinions__group\">
                                <div class=\"opinions__group-image\">
                                    <img src=\"assets/images/deorro.jpg\" alt=\"Imagen de usuario\">
                                </div>
                                <div class=\"opinions__group-text\">
                                    <h3 class=\"opinions__name\">Sergio Garnacho</h3>
                                    <h4 class=\"opinions__city\">Tetuán, Madrid</h4>
                                </div>                            
                            </div>
                        </div>
                    </li>
                    <li class=\"col-md-4 col-xs-12\">
                        <div class=\"opinions__content\">
                            <div class=\"opinions__text heading-3\">
                              <p>Nihil nam molestiae, animi ipsam ex labore? Expedita tenetur tempora mollitia, dolor blanditiis rerum eaque quia vero repellendus natus eveniet vel quam!</p>
                              <div class=\"triangulo-container\">
                                <div class=\"triangulo-base\"></div>
                                <div class=\"triangulo\"></div>
                              </div>
                            </div>
                            <div class=\"opinions__group\">
                                <div class=\"opinions__group-image\">
                                    <img src=\"assets/images/deorro.jpg\" alt=\"Imagen de usuario\">
                                </div>
                                <div class=\"opinions__group-text\">
                                    <h3 class=\"opinions__name\">Sergio Garnacho</h3>
                                    <h4 class=\"opinions__city\">Tetuán, Madrid</h4>
                                </div>                            
                            </div>
                        </div>
                    </li>
                    <li class=\"col-md-4 col-xs-12\">
                        <div class=\"opinions__content desktop\">
                            <div class=\"opinions__text heading-3\">
                                <p>Nihil nam molestiae, animi ipsam ex labore? Expedita tenetur tempora mollitia, dolor blanditiis rerum eaque quia vero repellendus natus eveniet vel quam!</p>
                                <div class=\"triangulo-container\">
                                    <div class=\"triangulo-base\"></div>
                                    <div class=\"triangulo\"></div>
                                </div>
                            </div>
                            <div class=\"opinions__group\">
                                <div class=\"opinions__group-image\">
                                    <img src=\"assets/images/deorro.jpg\" alt=\"Imagen de usuario\">
                                </div>
                                <div class=\"opinions__group-text\">
                                    <h3 class=\"opinions__name\">Sergio Garnacho</h3>
                                    <h4 class=\"opinions__city\">Tetuán, Madrid</h4>
                                </div>                            
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class=\"map\">
                <h2 class=\"function__title heading-2\">Guardianes cerca tuyo</h2>
                <div class=\"map__container\">
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3035.74804981012!2d-3.6968836846027986!3d40.45871307936029!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4229036539b84b%3A0xe9147f67419c2744!2sCalle%20de%20Orense%2C%2069%2C%2028020%20Madrid!5e0!3m2!1ses!2ses!4v1575818761089!5m2!1ses!2ses\" width=\"80%\" height=\"300\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
                </div>
            </div>
        </div>
    </div>
    <footer class=\"footer\">
        <div class=\"footer__content\">
            <div class=\"footer__logo\">
                <img src=\"assets/images/logo_blanco.svg\" alt=\"Maleteo\">
            </div>
            <p class=\"footer__text\">Hecho con <img src=\"assets/images/corazones.png\" alt=\"Corazón\"> en Madrid</p>
        </div>
    </footer>
</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "maleteo.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,700&display=swap\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"assets/styles/styles.css\">
</head>
<body>
    <div class=\"wrapper\">
        <div class=\"content-fluid\">
            <header class=\"header\">
                <div class=\"header__image\">
                    <img src=\"assets/images/logo.svg\"
                         alt=\"Maleteo\">
                </div>
            </header>
            <div class=\"intro\">
                <div class=\"row\">
                    <div class=\"col-md-6  col-xs-12\">
                        <div class=\"intro__content\">
                            <div class=\"intro__things\">
                                <h1 class=\"intro__title heading-1\">Gana dinero guardando equipaje a viajeros como tú</h1>
                                <ul class=\"intro__app-images\">
                                    <li class=\"intro__app-item\">
                                        <img src=\"assets/images/app-store.svg\" alt=\"App store\" class=\"intro__app-btn\">
                                    </li>
                                    <li class=\"intro__app-item\">
                                        <img src=\"assets/images/google-play.svg\" alt=\"Google play\" class=\"intro__app-btn\">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"function\">
                <h2 class=\"function__title heading-2\">¿Cómo funciona?</h2>
                <ul class=\"function__list row\">
                    <li class=\"col-xs-12 col-md-4\">
                        <div class=\"function__block\">
                            <div class=\"ball\">1</div>
                            <div class=\"function__text-content\">
                                <h2 class=\"function__subtitle\">Date de alta</h2>
                                <p class=\"function__text\">Lorem ipsum dolor sit, recusandae corporis libero cupiditate qui facilis voluptatibus et perferendis. Corporis non ullam voluptatem doloremque accusantium nam eveniet repellat.</p>
                            </div>
                        </div>
                    </li>
                    <li class=\"col-xs-12 col-md-4\">
                        <div class=\"function__block\">
                            <div class=\"ball\">2</div>
                            <div class=\"function__text-content\">
                                <h2 class=\"function__subtitle\">Publica tus espacios, horarios y tarifas</h2>
                                <p class=\"function__text\">Lorem ipsum dolor sit, recusandae corporis libero cupiditate qui facilis voluptatibus et perferendis. Corporis non ullam voluptatem doloremque accusantium nam eveniet repellat.</p>
                            </div>
                        </div>
                    </li>
                    <li class=\"col-xs-12 col-md-4\">
                        <div class=\"function__block\">
                            <div class=\"ball\">3</div>
                            <div class=\"function__text-content\">
                                <h2 class=\"function__subtitle\">Recibe viajeros y gana dinero</h2>
                                <p class=\"function__text\">Lorem ipsum dolor sit, recusandae corporis libero cupiditate qui facilis voluptatibus et perferendis. Corporis non ullam voluptatem doloremque accusantium nam eveniet repellat.</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class=\"form-image\">
                <div class=\"row\">
                    <div class=\"col-md-5\">
                        <div class=\"form-image__image\">
                            <img src=\"assets/images/iPhoneX.png\" alt=\"Image de un móvil\">
                        </div>
                    </div>
                    <div class=\"col-md-6 col-md-offset-1 col-xs-12\">
                        <div class=\"form-image__form\">
                            <div class=\"form-container\">
                                <p class=\"form-image__title heading-1\">Solicita una demo</p>
                                <form action=\"\" method=\"\">
                                    <div class=\"form-image__label-form heading-3\">
                                        <label class=\"label-form__text\" for=\"name\">Nombre</label>
                                        <input type=\"text\">
                                    </div>
                                    <div class=\"form-image__label-form heading-3\">
                                        <label class=\"label-form__text\" for=\"email\">Email</label>
                                        <input type=\"email\">
                                    </div>
                                    <div class=\"form-image__label-form heading-3\">
                                        <label class=\"label-form__text\" for=\"name\">Horario preferido</label>
                                        <input type=\"text\">
                                    </div>
                                    <div class=\"form-image__label-form heading-3\">
                                        <label class=\"label-form__text\" for=\"city\">Ciudad</label>
                                        <select name=\"city\" id=\"city\">
                                            <option value=\"\">Madrid</option>
                                        </select>
                                    </div>
                                    <div class=\"form-image__privacity heading-3\">
                                        <input type=\"checkbox\">
                                        <label>He leído y acepto la Política de Privacidad</label>
                                    </div>
                                    <div>
                                        <a href=\"#\" class=\"form-image__btn\">Enviar</a>
                                    </div>
                                </form>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"opinions\">
                <h2 class=\"function__title heading-2\">Opiniones de otros guardianes</h2>
                <ul class=\"row\">
                    <li class=\"col-md-4 col-xs-12\">
                        <div class=\"opinions__content\">
                            <div class=\"opinions__text heading-3\">
                                <p>Nihil nam molestiae, animi ipsam ex labore? Expedita tenetur tempora mollitia, dolor blanditiis rerum eaque quia vero repellendus natus eveniet vel quam!</p>
                                <div class=\"triangulo-container\">
                                    <div class=\"triangulo-base\"></div>
                                    <div class=\"triangulo\"></div>
                                </div>
                            </div>
                            <div class=\"opinions__group\">
                                <div class=\"opinions__group-image\">
                                    <img src=\"assets/images/deorro.jpg\" alt=\"Imagen de usuario\">
                                </div>
                                <div class=\"opinions__group-text\">
                                    <h3 class=\"opinions__name\">Sergio Garnacho</h3>
                                    <h4 class=\"opinions__city\">Tetuán, Madrid</h4>
                                </div>                            
                            </div>
                        </div>
                    </li>
                    <li class=\"col-md-4 col-xs-12\">
                        <div class=\"opinions__content\">
                            <div class=\"opinions__text heading-3\">
                              <p>Nihil nam molestiae, animi ipsam ex labore? Expedita tenetur tempora mollitia, dolor blanditiis rerum eaque quia vero repellendus natus eveniet vel quam!</p>
                              <div class=\"triangulo-container\">
                                <div class=\"triangulo-base\"></div>
                                <div class=\"triangulo\"></div>
                              </div>
                            </div>
                            <div class=\"opinions__group\">
                                <div class=\"opinions__group-image\">
                                    <img src=\"assets/images/deorro.jpg\" alt=\"Imagen de usuario\">
                                </div>
                                <div class=\"opinions__group-text\">
                                    <h3 class=\"opinions__name\">Sergio Garnacho</h3>
                                    <h4 class=\"opinions__city\">Tetuán, Madrid</h4>
                                </div>                            
                            </div>
                        </div>
                    </li>
                    <li class=\"col-md-4 col-xs-12\">
                        <div class=\"opinions__content desktop\">
                            <div class=\"opinions__text heading-3\">
                                <p>Nihil nam molestiae, animi ipsam ex labore? Expedita tenetur tempora mollitia, dolor blanditiis rerum eaque quia vero repellendus natus eveniet vel quam!</p>
                                <div class=\"triangulo-container\">
                                    <div class=\"triangulo-base\"></div>
                                    <div class=\"triangulo\"></div>
                                </div>
                            </div>
                            <div class=\"opinions__group\">
                                <div class=\"opinions__group-image\">
                                    <img src=\"assets/images/deorro.jpg\" alt=\"Imagen de usuario\">
                                </div>
                                <div class=\"opinions__group-text\">
                                    <h3 class=\"opinions__name\">Sergio Garnacho</h3>
                                    <h4 class=\"opinions__city\">Tetuán, Madrid</h4>
                                </div>                            
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class=\"map\">
                <h2 class=\"function__title heading-2\">Guardianes cerca tuyo</h2>
                <div class=\"map__container\">
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3035.74804981012!2d-3.6968836846027986!3d40.45871307936029!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4229036539b84b%3A0xe9147f67419c2744!2sCalle%20de%20Orense%2C%2069%2C%2028020%20Madrid!5e0!3m2!1ses!2ses!4v1575818761089!5m2!1ses!2ses\" width=\"80%\" height=\"300\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
                </div>
            </div>
        </div>
    </div>
    <footer class=\"footer\">
        <div class=\"footer__content\">
            <div class=\"footer__logo\">
                <img src=\"assets/images/logo_blanco.svg\" alt=\"Maleteo\">
            </div>
            <p class=\"footer__text\">Hecho con <img src=\"assets/images/corazones.png\" alt=\"Corazón\"> en Madrid</p>
        </div>
    </footer>
</body>
</html>", "maleteo.html.twig", "/application/templates/maleteo.html.twig");
    }
}
