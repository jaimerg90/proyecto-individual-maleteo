<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* maleteo.html.twig */
class __TwigTemplate_5286c7d6130b8e00428686eb41fab327ff37cc4a486ae0115318f42a30791fdf extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'header' => [$this, 'block_header'],
            'footer' => [$this, 'block_footer'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "maleteo.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "maleteo.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,700&display=swap\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"assets/styles/styles.css\">
    <script src= \"/scripts/scripts.js\" type=\"text/javascript\"></script>
    <script src=\"https://cdn.jsdelivr.net/npm/squirrelly@7.5.0/dist/squirrelly.min.js\"></script>
</head>
<body>
    <div class=\"wrapper\">
        <div class=\"content-fluid\">
            ";
        // line 16
        $this->displayBlock('header', $context, $blocks);
        // line 27
        echo "            <div class=\"intro\">
                <div class=\"row\">
                    <div class=\"col-md-6  col-xs-12\">
                        <div class=\"intro__content\">
                            <div class=\"intro__things\">
                                <h1 class=\"intro__title heading-1\">Gana dinero guardando equipaje a viajeros como tú</h1>
                                <ul class=\"intro__app-images\">
                                    <li class=\"intro__app-item\">
                                        <img src=\"assets/images/app-store.svg\" alt=\"App store\" class=\"intro__app-btn\">
                                    </li>
                                    <li class=\"intro__app-item\">
                                        <img src=\"assets/images/google-play.svg\" alt=\"Google play\" class=\"intro__app-btn\">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"function\">
                <h2 class=\"function__title heading-2\">¿Cómo funciona?</h2>
                <ul class=\"function__list row\">
                    <li class=\"col-xs-12 col-md-4\">
                        <div class=\"function__block\">
                            <div class=\"ball\">1</div>
                            <div class=\"function__text-content\">
                                <h2 class=\"function__subtitle\">Date de alta</h2>
                                <p class=\"function__text\">Lorem ipsum dolor sit, recusandae corporis libero cupiditate qui facilis voluptatibus et perferendis. Corporis non ullam voluptatem doloremque accusantium nam eveniet repellat.</p>
                            </div>
                        </div>
                    </li>
                    <li class=\"col-xs-12 col-md-4\">
                        <div class=\"function__block\">
                            <div class=\"ball\">2</div>
                            <div class=\"function__text-content\">
                                <h2 class=\"function__subtitle\">Publica tus espacios, horarios y tarifas</h2>
                                <p class=\"function__text\">Lorem ipsum dolor sit, recusandae corporis libero cupiditate qui facilis voluptatibus et perferendis. Corporis non ullam voluptatem doloremque accusantium nam eveniet repellat.</p>
                            </div>
                        </div>
                    </li>
                    <li class=\"col-xs-12 col-md-4\">
                        <div class=\"function__block\">
                            <div class=\"ball\">3</div>
                            <div class=\"function__text-content\">
                                <h2 class=\"function__subtitle\">Recibe viajeros y gana dinero</h2>
                                <p class=\"function__text\">Lorem ipsum dolor sit, recusandae corporis libero cupiditate qui facilis voluptatibus et perferendis. Corporis non ullam voluptatem doloremque accusantium nam eveniet repellat.</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class=\"form-image\">
                <div class=\"row\">
                    <div class=\"col-md-5\">
                        <div class=\"form-image__image\">
                            <img src=\"assets/images/iPhoneX.png\" alt=\"Image de un móvil\">
                        </div>
                    </div>
                    <div class=\"col-md-6 col-md-offset-1 col-xs-12\">
                        <div id=\"aaa\">
                            <div class=\"form-image__form\" id=\"formulariopint\">
                                <div class=\"form-container\">
                                    <p class=\"form-image__title heading-1\">Solicita una demo</p>
                                    ";
        // line 90
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 90, $this->source); })()), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage"), "attr" => ["id" => "formulario"]]);
        echo "
                                        <div class=\"form-image__label-form heading-3\">
                                            ";
        // line 92
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 92, $this->source); })()), "nombre", [], "any", false, false, false, 92), 'label');
        echo "
                                            ";
        // line 93
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 93, $this->source); })()), "nombre", [], "any", false, false, false, 93), 'widget');
        echo "
                                            ";
        // line 96
        echo "                                        </div>
                                        <div class=\"form-image__label-form heading-3\">
                                            ";
        // line 98
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 98, $this->source); })()), "email", [], "any", false, false, false, 98), 'label');
        echo "
                                            ";
        // line 99
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 99, $this->source); })()), "email", [], "any", false, false, false, 99), 'widget');
        echo "
                                            ";
        // line 102
        echo "                                        </div>
                                        <div class=\"form-image__label-form heading-3\">
                                            ";
        // line 104
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 104, $this->source); })()), "horario", [], "any", false, false, false, 104), 'label');
        echo "
                                            ";
        // line 105
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 105, $this->source); })()), "horario", [], "any", false, false, false, 105), 'widget');
        echo "
                                            ";
        // line 108
        echo "                                        </div>
                                        <div class=\"form-image__label-form heading-3\">
                                            ";
        // line 110
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 110, $this->source); })()), "ciudad", [], "any", false, false, false, 110), 'label');
        echo "
                                            ";
        // line 111
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 111, $this->source); })()), "ciudad", [], "any", false, false, false, 111), 'widget');
        echo "
                                            ";
        // line 116
        echo "                                        </div>
                                        <div class=\"form-image__privacity heading-3\">
                                            ";
        // line 118
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 118, $this->source); })()), "privacidad", [], "any", false, false, false, 118), 'widget');
        echo "
                                            ";
        // line 119
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 119, $this->source); })()), "privacidad", [], "any", false, false, false, 119), 'label');
        echo "
                                            ";
        // line 122
        echo "                                        </div>
                                        <div>
                                            ";
        // line 124
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 124, $this->source); })()), "Enviar", [], "any", false, false, false, 124), 'widget', ["attr" => ["class" => "form-image__btn"]]);
        echo "
                                            ";
        // line 126
        echo "                                        </div>
                                    ";
        // line 127
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 127, $this->source); })()), 'form_end');
        echo "
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"opinions\">
                <h2 class=\"function__title heading-2\">Opiniones de otros guardianes</h2>
                <ul class=\"row\">
                    ";
        // line 137
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["opinion"]) || array_key_exists("opinion", $context) ? $context["opinion"] : (function () { throw new RuntimeError('Variable "opinion" does not exist.', 137, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["op"]) {
            // line 138
            echo "                        <li class=\"col-md-4 col-xs-12\">
                            <div class=\"opinions__content\">
                                <div class=\"opinions__text heading-3\">
                                    <p>";
            // line 141
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["op"], "comentario", [], "any", false, false, false, 141), "html", null, true);
            echo "</p>
                                    <div class=\"triangulo-container\">
                                        <div class=\"triangulo-base\"></div>
                                        <div class=\"triangulo\"></div>
                                    </div>
                                </div>
                                <div class=\"opinions__group\">
                                    <div class=\"opinions__group-image\">
                                        <img src=\"assets/images/deorro.jpg\" alt=\"Imagen de usuario\">
                                    </div>
                                    <div class=\"opinions__group-text\">
                                        <h3 class=\"opinions__name\">";
            // line 152
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["op"], "nombre", [], "any", false, false, false, 152), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["op"], "apellido", [], "any", false, false, false, 152), "html", null, true);
            echo "</h3> 
                                        <h4 class=\"opinions__city\">";
            // line 153
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["op"], "barrio", [], "any", false, false, false, 153), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["op"], "ciudad", [], "any", false, false, false, 153), "html", null, true);
            echo "</h4>
                                    </div>                            
                                </div>
                            </div>
                        </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['op'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 159
        echo "                </ul>
            </div>
            <div class=\"map\">
                <h2 class=\"function__title heading-2\">Guardianes cerca tuyo</h2>
                <div class=\"row\">
                    <div class=\"col-md-3\" id=\"map__users\" class=\"map__users\">
                    </div>
                    <div class=\"col-md-9\"> 
                        <div class=\"map__container\">
                            <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3035.74804981012!2d-3.6968836846027986!3d40.45871307936029!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4229036539b84b%3A0xe9147f67419c2744!2sCalle%20de%20Orense%2C%2069%2C%2028020%20Madrid!5e0!3m2!1ses!2ses!4v1575818761089!5m2!1ses!2ses\" width=\"80%\" height=\"300\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    ";
        // line 175
        $this->displayBlock('footer', $context, $blocks);
        // line 185
        echo "</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 16
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 17
        echo "            <header class=\"header\">
                <div class=\"header__image\">
                    <img src=\"assets/images/logo.svg\"
                         alt=\"Maleteo\">
                </div>
                <div class=\"inicio-sesion\">
                    <a href=\"";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login-usuario");
        echo "\" class=\"form-image__btn\">Inicia sesión</a>
                </div>
            </header>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 175
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        // line 176
        echo "    <footer class=\"footer\">
        <div class=\"footer__content\">
            <div class=\"footer__logo\">
                <img src=\"assets/images/logo_blanco.svg\" alt=\"Maleteo\">
            </div>
            <p class=\"footer__text\">Hecho con <img src=\"assets/images/corazones.png\" alt=\"Corazón\"> en Madrid</p>
        </div>
    </footer>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "maleteo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  330 => 176,  320 => 175,  306 => 23,  298 => 17,  288 => 16,  277 => 185,  275 => 175,  257 => 159,  243 => 153,  237 => 152,  223 => 141,  218 => 138,  214 => 137,  201 => 127,  198 => 126,  194 => 124,  190 => 122,  186 => 119,  182 => 118,  178 => 116,  174 => 111,  170 => 110,  166 => 108,  162 => 105,  158 => 104,  154 => 102,  150 => 99,  146 => 98,  142 => 96,  138 => 93,  134 => 92,  129 => 90,  64 => 27,  62 => 16,  45 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,700&display=swap\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"assets/styles/styles.css\">
    <script src= \"/scripts/scripts.js\" type=\"text/javascript\"></script>
    <script src=\"https://cdn.jsdelivr.net/npm/squirrelly@7.5.0/dist/squirrelly.min.js\"></script>
</head>
<body>
    <div class=\"wrapper\">
        <div class=\"content-fluid\">
            {% block header %}
            <header class=\"header\">
                <div class=\"header__image\">
                    <img src=\"assets/images/logo.svg\"
                         alt=\"Maleteo\">
                </div>
                <div class=\"inicio-sesion\">
                    <a href=\"{{ path('login-usuario') }}\" class=\"form-image__btn\">Inicia sesión</a>
                </div>
            </header>
            {% endblock header %}
            <div class=\"intro\">
                <div class=\"row\">
                    <div class=\"col-md-6  col-xs-12\">
                        <div class=\"intro__content\">
                            <div class=\"intro__things\">
                                <h1 class=\"intro__title heading-1\">Gana dinero guardando equipaje a viajeros como tú</h1>
                                <ul class=\"intro__app-images\">
                                    <li class=\"intro__app-item\">
                                        <img src=\"assets/images/app-store.svg\" alt=\"App store\" class=\"intro__app-btn\">
                                    </li>
                                    <li class=\"intro__app-item\">
                                        <img src=\"assets/images/google-play.svg\" alt=\"Google play\" class=\"intro__app-btn\">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"function\">
                <h2 class=\"function__title heading-2\">¿Cómo funciona?</h2>
                <ul class=\"function__list row\">
                    <li class=\"col-xs-12 col-md-4\">
                        <div class=\"function__block\">
                            <div class=\"ball\">1</div>
                            <div class=\"function__text-content\">
                                <h2 class=\"function__subtitle\">Date de alta</h2>
                                <p class=\"function__text\">Lorem ipsum dolor sit, recusandae corporis libero cupiditate qui facilis voluptatibus et perferendis. Corporis non ullam voluptatem doloremque accusantium nam eveniet repellat.</p>
                            </div>
                        </div>
                    </li>
                    <li class=\"col-xs-12 col-md-4\">
                        <div class=\"function__block\">
                            <div class=\"ball\">2</div>
                            <div class=\"function__text-content\">
                                <h2 class=\"function__subtitle\">Publica tus espacios, horarios y tarifas</h2>
                                <p class=\"function__text\">Lorem ipsum dolor sit, recusandae corporis libero cupiditate qui facilis voluptatibus et perferendis. Corporis non ullam voluptatem doloremque accusantium nam eveniet repellat.</p>
                            </div>
                        </div>
                    </li>
                    <li class=\"col-xs-12 col-md-4\">
                        <div class=\"function__block\">
                            <div class=\"ball\">3</div>
                            <div class=\"function__text-content\">
                                <h2 class=\"function__subtitle\">Recibe viajeros y gana dinero</h2>
                                <p class=\"function__text\">Lorem ipsum dolor sit, recusandae corporis libero cupiditate qui facilis voluptatibus et perferendis. Corporis non ullam voluptatem doloremque accusantium nam eveniet repellat.</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class=\"form-image\">
                <div class=\"row\">
                    <div class=\"col-md-5\">
                        <div class=\"form-image__image\">
                            <img src=\"assets/images/iPhoneX.png\" alt=\"Image de un móvil\">
                        </div>
                    </div>
                    <div class=\"col-md-6 col-md-offset-1 col-xs-12\">
                        <div id=\"aaa\">
                            <div class=\"form-image__form\" id=\"formulariopint\">
                                <div class=\"form-container\">
                                    <p class=\"form-image__title heading-1\">Solicita una demo</p>
                                    {{ form_start(formulario, {'action': path('homepage'),'attr': {'id': 'formulario'}}) }}
                                        <div class=\"form-image__label-form heading-3\">
                                            {{ form_label(formulario.nombre) }}
                                            {{ form_widget(formulario.nombre) }}
                                            {# <label class=\"label-form__text\" for=\"name\">Nombre</label>
                                            <input type=\"text\"> #}
                                        </div>
                                        <div class=\"form-image__label-form heading-3\">
                                            {{ form_label(formulario.email) }}
                                            {{ form_widget(formulario.email) }}
                                            {# <label class=\"label-form__text\" for=\"email\">Email</label>
                                            <input type=\"email\"> #}
                                        </div>
                                        <div class=\"form-image__label-form heading-3\">
                                            {{ form_label(formulario.horario) }}
                                            {{ form_widget(formulario.horario) }}
                                            {# <label class=\"label-form__text\" for=\"name\">Horario preferido</label>
                                            <input type=\"text\"> #}
                                        </div>
                                        <div class=\"form-image__label-form heading-3\">
                                            {{ form_label(formulario.ciudad) }}
                                            {{ form_widget(formulario.ciudad) }}
                                            {# <label class=\"label-form__text\" for=\"city\">Ciudad</label>
                                            <select name=\"city\" id=\"city\">
                                                <option value=\"\">Madrid</option>
                                            </select> #}
                                        </div>
                                        <div class=\"form-image__privacity heading-3\">
                                            {{ form_widget(formulario.privacidad) }}
                                            {{ form_label(formulario.privacidad) }}
                                            {# <input type=\"checkbox\">
                                            <label>He leído y acepto la Política de Privacidad</label> #}
                                        </div>
                                        <div>
                                            {{ form_widget(formulario.Enviar, {'attr': {'class': 'form-image__btn'}}) }}
                                            {# <a href=\"#\" class=\"form-image__btn\"></a> #}
                                        </div>
                                    {{ form_end(formulario) }}
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"opinions\">
                <h2 class=\"function__title heading-2\">Opiniones de otros guardianes</h2>
                <ul class=\"row\">
                    {% for op in opinion %}
                        <li class=\"col-md-4 col-xs-12\">
                            <div class=\"opinions__content\">
                                <div class=\"opinions__text heading-3\">
                                    <p>{{ op.comentario }}</p>
                                    <div class=\"triangulo-container\">
                                        <div class=\"triangulo-base\"></div>
                                        <div class=\"triangulo\"></div>
                                    </div>
                                </div>
                                <div class=\"opinions__group\">
                                    <div class=\"opinions__group-image\">
                                        <img src=\"assets/images/deorro.jpg\" alt=\"Imagen de usuario\">
                                    </div>
                                    <div class=\"opinions__group-text\">
                                        <h3 class=\"opinions__name\">{{op.nombre}} {{op.apellido}}</h3> 
                                        <h4 class=\"opinions__city\">{{op.barrio}}, {{op.ciudad}}</h4>
                                    </div>                            
                                </div>
                            </div>
                        </li>
                    {% endfor %}
                </ul>
            </div>
            <div class=\"map\">
                <h2 class=\"function__title heading-2\">Guardianes cerca tuyo</h2>
                <div class=\"row\">
                    <div class=\"col-md-3\" id=\"map__users\" class=\"map__users\">
                    </div>
                    <div class=\"col-md-9\"> 
                        <div class=\"map__container\">
                            <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3035.74804981012!2d-3.6968836846027986!3d40.45871307936029!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4229036539b84b%3A0xe9147f67419c2744!2sCalle%20de%20Orense%2C%2069%2C%2028020%20Madrid!5e0!3m2!1ses!2ses!4v1575818761089!5m2!1ses!2ses\" width=\"80%\" height=\"300\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {% block footer %}
    <footer class=\"footer\">
        <div class=\"footer__content\">
            <div class=\"footer__logo\">
                <img src=\"assets/images/logo_blanco.svg\" alt=\"Maleteo\">
            </div>
            <p class=\"footer__text\">Hecho con <img src=\"assets/images/corazones.png\" alt=\"Corazón\"> en Madrid</p>
        </div>
    </footer>
    {% endblock footer %}
</body>
</html>", "maleteo.html.twig", "/application/templates/maleteo.html.twig");
    }
}
